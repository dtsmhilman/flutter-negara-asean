const negaraAsean = [
  {
    'nama': 'Indonesia',
    'ibuKota': 'Jakarta',
    'bendera': 'assets/img/indonesia.png'
  },
  {
    'nama': 'Filipina',
    'ibuKota': 'Manila',
    'bendera': 'assets/img/philippines.png'
  },
  {
    'nama': 'Malaysia',
    'ibuKota': 'Kuala Lumpur',
    'bendera': 'assets/img/malaysia.png'
  },
  {
    'nama': 'Brunei Darussalam',
    'ibuKota': 'Bandar Seri Begawan',
    'bendera': 'assets/img/brunei.png'
  },
  {
    'nama': 'Kamboja',
    'ibuKota': 'Phnom Penh',
    'bendera': 'assets/img/cambodia.png'
  },
  {'nama': 'Laos', 'ibuKota': 'Vientiane', 'bendera': 'assets/img/laos.png'},
  {
    'nama': 'Myanmar',
    'ibuKota': 'Nay Pyi Taw',
    'bendera': 'assets/img/myanmar.png'
  },
  {
    'nama': 'Singapura',
    'ibuKota': 'Singapura',
    'bendera': 'assets/img/singapore.png'
  },
  {
    'nama': 'Thailand',
    'ibuKota': 'Bankok',
    'bendera': 'assets/img/thailand.png'
  },
  {'nama': 'Vietnam', 'ibuKota': 'Hanoi', 'bendera': 'assets/img/vietnam.png'}
];
