import 'package:flutter/material.dart';
import 'negara-asean.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Pengenalan_1',
      home: NegaraAsean(),
      theme: ThemeData(
        brightness: Brightness.dark,
      ),
    );
  }
}

class NegaraAsean extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Negara Asean'),
      ),
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              for (var items in negaraAsean)
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 16),
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: new Image.asset(items['bendera'],
                            height: 50, width: 100, fit: BoxFit.cover),
                      ),
                      Expanded(
                        child: Container(
                            margin: EdgeInsets.only(left: 16.0),
                            height: 70,
                            decoration: BoxDecoration(
                                color: Color(0xff3f3f3f),
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(8)),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    items['nama'].toUpperCase(),
                                    style: TextStyle(
                                        fontFamily: 'RobotoMono',
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    items['ibuKota'],
                                    style: TextStyle(
                                        fontFamily: 'RobotoMono', fontSize: 15),
                                  )
                                ])),
                      )
                    ],
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}
